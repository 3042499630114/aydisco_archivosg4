FROM python:3.9.17

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN pip install -r requirements.txt

COPY . .

EXPOSE 7777
CMD ["python", "api/app.py"]