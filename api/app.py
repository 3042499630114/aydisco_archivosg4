import json
import urllib.parse

from bson import ObjectId
from flask import Flask, request

from dotenv import load_dotenv
import boto3
from boto3.s3.transfer import S3Transfer
from encoder import MyEncoder
import os
import jwt
from pymongo import MongoClient
from flask_cors import CORS

load_dotenv()

UPLOAD_FOLDER = os.getcwd() + '/../upload'
ALLOWED_EXTENSIONS = {'mp3'}
JWT_SECRET_KEY = 'b2d5dcba-bc91-43e3-960e-a208e8e024f7'

ATLAS_URI = 'mongodb+srv://ay-discos:60Y30Uvclt5aQtNR@cluster0.qpiimgs.mongodb.net/ay-discos?retryWrites=true&w=majority'


def upload_file(client, filename, file_path, album_id):
    if os.path.isfile(file_path):
        transfer = S3Transfer(client)

        bucket_name = "canciones-s3-p1-g4-ayd"
        key = "media/{}/{}".format(album_id, filename)
        transfer.upload_file(file_path, bucket_name, key)
        return '%s/%s/%s' % (client.meta.endpoint_url, bucket_name, key)
    else:
        return None


def download_file(client, filename):
    if os.path.isfile(filename):
        bucket_name = "canciones-s3-p1-g4-ayd"
        key = "media/{}".format(filename)
        client.download_file(bucket_name, key, filename)
        return True
    else:
        return False


app = Flask(__name__)
cors = CORS(app)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SECRET_KEY'] = JWT_SECRET_KEY
app.config['ATLAS_URL'] = ATLAS_URI

mongodb_client = MongoClient(app.config["ATLAS_URL"])
db = mongodb_client['ay-discos']
db_canciones = db.canciones

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def hello():
    return 'Hola desde FLask'


@app.route('/v1/files', methods=['POST'])
def upload():

    try:
        accessToken = request.headers['authorization']
        user = jwt.decode(accessToken, app.config['SECRET_KEY'], algorithms=["HS256"])
    except:
        no_autorizado = {
            "error": "Token invalido"
        }
        response = app.response_class(
            response=json.dumps(no_autorizado, cls=MyEncoder),
            status=401,
            mimetype='application/json'
        )
        return response

    file = request.files['file']
    filename = file.filename
    file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    file.save(os.path.join(file_path))

    titulo = request.form.get('titulo')
    album_id = request.form.get('album_id')

    aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID')
    aws_secret_access_key = os.getenv('AWS_SECRET_ACCESS_KEY')

    client = boto3.client('s3',
                          aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key,
                          )

    link = upload_file(client, filename, file_path, album_id)
    link = urllib.parse.quote_plus(link)

    os.remove(os.path.join(file_path))

    if link is not None:
        cancion = {
            "titulo": titulo,
            "album_id": album_id,
            "user_id": user['_id'],
            "s3_link": link
        }
        db_canciones.insert_one(cancion)

        response = app.response_class(
            response=json.dumps(cancion, cls=MyEncoder),
            status=200,
            mimetype='application/json'
        )
        return response
    else:
        errortz = {
            "error": "No se pudo subir a S3"
        }
        response = app.response_class(
            response=json.dumps(errortz, cls=MyEncoder),
            status=500,
            mimetype='application/json'
        )
        return response


@app.route('/v1/files/<file_id>', methods=['GET'])
def download(file_id):
    try:
        accessToken = request.headers['authorization']
        user = jwt.decode(accessToken, app.config['SECRET_KEY'], algorithms=["HS256"])
    except:
        no_autorizado = {
            "error": "Token invalido"
        }
        response = app.response_class(
            response=json.dumps(no_autorizado, cls=MyEncoder),
            status=401,
            mimetype='application/json'
        )
        return response

    user_id = user['_id']
    cancion = db_canciones.find_one({
        "_id": ObjectId(file_id),
        "user_id": user_id
    })

    link = cancion['s3_link']

    link_respuesta = {
        "link": link
    }
    response = app.response_class(
        response=json.dumps(link_respuesta, cls=MyEncoder),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/v1/files/album/<album_id>', methods=['GET'])
def get_canciones_album(album_id):
    print(album_id)
    try:
        accessToken = request.headers['authorization']
        user = jwt.decode(accessToken, app.config['SECRET_KEY'], algorithms=["HS256"])
    except:
        no_autorizado = {
            "error": "Token invalido"
        }
        response = app.response_class(
            response=json.dumps(no_autorizado, cls=MyEncoder),
            status=401,
            mimetype='application/json'
        )
        return response

    user_id = user['_id']
    canciones_crs = db_canciones.find({
        "album_id": album_id,
        "user_id": user_id
    })

    canciones = []
    for cancion in canciones_crs:
        print(cancion)
        canciones.append(cancion)

    response = app.response_class(
        response=json.dumps(canciones, cls=MyEncoder),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/v1/files/<file_id>', methods=['DELETE'])
def delete(file_id):
    try:
        accessToken = request.headers['authorization']
        user = jwt.decode(accessToken, app.config['SECRET_KEY'], algorithms=["HS256"])
    except:
        no_autorizado = {
            "error": "Token invalido"
        }
        response = app.response_class(
            response=json.dumps(no_autorizado, cls=MyEncoder),
            status=401,
            mimetype='application/json'
        )
        return response

    user_id = user['_id']
    cancion = db_canciones.find_one({
        "_id": ObjectId(file_id),
        "user_id": user_id
    })

    if cancion is None:
        archivo_no_encontrado = {
            "error": "El archivo no existe"
        }
        response = app.response_class(
            response=json.dumps(archivo_no_encontrado, cls=MyEncoder),
            status=404,
            mimetype='application/json'
        )
        return response

    link = cancion['s3_link']

    aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID')
    aws_secret_access_key = os.getenv('AWS_SECRET_ACCESS_KEY')

    # Eliminar archivo del bucket de AWS
    client = boto3.client('s3',
                          aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key
                          )
    bucket_name = "canciones-s3-p1-g4-ayd"
    key = urllib.parse.unquote(link.split('/')[-1])
    client.delete_object(Bucket=bucket_name, Key=key)

    # Eliminar documento de la colección en MongoDB
    db_canciones.delete_one({"_id": cancion["_id"]})

    respuesta = {
        "message": "Archivo eliminado exitosamente"
    }
    response = app.response_class(
        response=json.dumps(respuesta, cls=MyEncoder),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/v1/files/update/<file_id>', methods=['PUT'])
def update(file_id):
    try:
        accessToken = request.headers['authorization']
        user = jwt.decode(accessToken, app.config['SECRET_KEY'], algorithms=["HS256"])
    except:
        no_autorizado = {
            "error": "Token invalido"
        }
        response = app.response_class(
            response=json.dumps(no_autorizado, cls=MyEncoder),
            status=401,
            mimetype='application/json'
        )
        return response

    user_id = user['_id']
    cancion = db_canciones.find_one({
        "_id": ObjectId(file_id),
        "user_id": user_id
    })

    if cancion is None:
        archivo_no_encontrado = {
            "error": "El archivo no existe"
        }
        response = app.response_class(
            response=json.dumps(archivo_no_encontrado, cls=MyEncoder),
            status=404,
            mimetype='application/json'
        )
        return response

    # Obtener el nuevo archivo desde la solicitud
    new_file = request.files['file']
    new_filename = new_file.filename
    new_file_path = os.path.join(app.config['UPLOAD_FOLDER'], new_filename)
    new_file.save(new_file_path)

    # Actualizar el archivo en S3
    aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID')
    aws_secret_access_key = os.getenv('AWS_SECRET_ACCESS_KEY')

    client = boto3.client('s3',
                          aws_access_key_id=aws_access_key_id,
                          aws_secret_access_key=aws_secret_access_key,
                          )

    # Eliminar el archivo anterior de S3
    old_link = cancion['s3_link']
    old_key = urllib.parse.unquote(old_link.split('/')[-1])
    bucket_name = "canciones-s3-p1-g4-ayd"
    client.delete_object(Bucket=bucket_name, Key=old_key)

    # Subir el nuevo archivo a S3
    new_link = upload_file(client, new_filename, new_file_path, cancion['album_id'])
    new_link = urllib.parse.quote_plus(new_link)

    os.remove(new_file_path)

    if new_link is not None:
        # Actualizar la información del archivo en la colección de MongoDB
        updated_cancion = {
            "titulo": request.form.get('titulo', cancion['titulo']),
            "album_id": cancion['album_id'],
            "user_id": cancion['user_id'],
            "s3_link": new_link
        }
        db_canciones.update_one({"_id": cancion["_id"]}, {"$set": updated_cancion})

        response = app.response_class(
            response=json.dumps(updated_cancion, cls=MyEncoder),
            status=200,
            mimetype='application/json'
        )
        return response
    else:
        errortz = {
            "error": "No se pudo subir el archivo modificado a S3"
        }
        response = app.response_class(
            response=json.dumps(errortz, cls=MyEncoder),
            status=500,
            mimetype='application/json'
        )
        return response




if __name__ == '__main__':
    app.run(host='0.0.0.0', port=7777)
