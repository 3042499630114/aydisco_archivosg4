import uuid
from typing import Optional
from pydantic import BaseModel, Field


class Cancion(BaseModel):
    id: str = Field(default_factory=uuid.uuid4, alias="_id")
    title: str = Field(...)
    s3_link: str = Field(...)
    album_id: str = Field(...)
    user_id: str = Field(...)

    class Config:
        allow_population_by_field_name = True
        schema_extra = {
            "example": {
                "_id": "066de609-b04a-4b30-b46c-32537c7f1f6e",
                "title": "Cancion",
                "s3_link": "https://s3....",
                "album_id": "0988u4g090443grefdbd6t34er",
                "user_id": "9u049cu098j09384tf24rfe"
            }
        }