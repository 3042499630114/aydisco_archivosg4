from dotenv import load_dotenv
import boto3
import os
import botocore
import uuid

load_dotenv()

upsong = 'cancion.mp3'
downsong = '8479e539-6d5d-4167-ac7d-ca8100b35bdd.mp3'

def generate_unique_id():
    return str(uuid.uuid4())

def upload_file(client):
    if os.path.isfile(upsong):
        filename = upsong
        bucket_name = "canciones-s3-p1-g4-ayd"
        file_id = generate_unique_id()
        key = f"media/{file_id}.mp3"
        client.upload_file(filename, bucket_name, key)
        print(f"Canción subida con ID: {file_id}")
    else:
        print(f"No se encontró el archivo: {upsong}")

def download_file(client):
    file_id = downsong
    bucket_name = "canciones-s3-p1-g4-ayd"
    key = f"media/{file_id}"
    try:
        client.download_file(bucket_name, key, file_id)
        print("Canción descargada exitosamente")
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print(f"No se encontró el archivo en S3: {key}")
        else:
            print("Ocurrió un error durante la descarga")

if __name__ == "__main__":
    aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID')
    aws_secret_access_key = os.getenv('AWS_SECRET_ACCESS_KEY')

    client = boto3.client('s3',
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
    )

    #upload_file(client)
    download_file(client)
